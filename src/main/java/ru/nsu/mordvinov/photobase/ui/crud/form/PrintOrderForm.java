package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.PrintOrderCrudLogic;

import java.time.LocalDateTime;


public class PrintOrderForm extends BaseForm<PrintOrder> {

    private TextField comment;

    private ComboBox<Client> client;
    private ComboBox<Employee> employee;

    private Checkbox isDone;

    private PrintOrderCrudLogic viewLogic;

    public PrintOrderForm(PrintOrderCrudLogic viewLogic) {
        super(viewLogic, PrintOrder.class, PrintOrder::new, PrintOrder::getId);
        this.viewLogic = viewLogic;

        client = new ComboBox<>("Client");
        client.setWidth("100%");
        client.setRequired(true);
        client.setItems(viewLogic.getClientList());
        client.setItemLabelGenerator(Client::gridFormatted);
        getContent().add(client);

        employee = new ComboBox<>("Employee");
        employee.setWidth("100%");
        employee.setRequired(true);
        employee.setItems(viewLogic.getEmployeeList());
        employee.setItemLabelGenerator(Employee::gridFormatted);
        getContent().add(employee);

        comment = new TextField("Comment");
        comment.setWidth("100%");
        comment.setRequired(true);
        comment.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(comment);


        isDone = new Checkbox("Order done");
        isDone.setWidth("100%");
        getContent().add(isDone);

        bindFields();
    }

    @Override
    protected void customBeforeSave() {
        if (getCurrentEntity().getDate() == null) {
            getCurrentEntity().setDate(LocalDateTime.now());
        }
    }
}
