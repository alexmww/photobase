package ru.nsu.mordvinov.photobase.backend.repository;

import org.springframework.stereotype.Repository;
import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;


@Repository
public interface PrintOrderRepository extends BaseRepository<PrintOrder> {
}