package ru.nsu.mordvinov.photobase.backend.model.enums;

import lombok.Getter;

import java.io.Serializable;

public enum Format implements Serializable {
    F16_9("16x9"),
    F21_7("21x7");

    @Getter
    private String description;

    Format(String description) {
        this.description = description;
    }
}
