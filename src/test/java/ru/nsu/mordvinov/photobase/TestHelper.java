package ru.nsu.mordvinov.photobase;

import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.model.Office;

public class TestHelper {

    public static Office createOffice(){
        Office office = new Office();
        office.setAddress("Lenina, 9");
        office.setName("Test office");
        return office;
    }

    public static Branch createBranch(){
        Branch branch = new Branch();
        branch.setAddress("Lenina, 11");
        branch.setName("Test branch");
        return branch;
    }

    public static Kiosk createKiosk(){
        Kiosk kiosk = new Kiosk();
        kiosk.setAddress("Lenina, 13");
        kiosk.setName("Test branch");
        return kiosk;
    }
}
