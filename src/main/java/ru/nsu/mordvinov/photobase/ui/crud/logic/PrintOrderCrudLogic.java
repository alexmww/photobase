package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;
import ru.nsu.mordvinov.photobase.backend.repository.ClientRepository;
import ru.nsu.mordvinov.photobase.backend.repository.EmployeeRepository;
import ru.nsu.mordvinov.photobase.backend.repository.PrintOrderRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.PrintOrderCrudView;

import java.util.List;

@Slf4j
public class PrintOrderCrudLogic extends BaseCrudLogic<PrintOrder> {

    private PrintOrderCrudView view;

    private final PrintOrderRepository printOrderRepository;
    private final ClientRepository clientRepository;
    private final EmployeeRepository employeeRepository;

    public PrintOrderCrudLogic(PrintOrderCrudView printOrderCrudView,
                               PrintOrderRepository printOrderRepository,
                               ClientRepository clientRepository,
                               EmployeeRepository employeeRepository) {
        super(printOrderCrudView,
                PrintOrderCrudView.class,
                printOrderRepository,
                PrintOrder::getId,
                PrintOrder::new,
                o -> "Order id: " + o.getId());
        view = printOrderCrudView;
        this.printOrderRepository = printOrderRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Client> getClientList() {
        return clientRepository.findAll();
    }

    public List<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }
}
