package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.repository.EmployeeRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class EmployeeDataProvider extends BaseProvider<Employee> {


    public EmployeeDataProvider(EmployeeRepository employeeRepository) {
        super(employeeRepository, Employee::getId);
    }

    @Override
    protected SerializablePredicate<Employee> filter() {
        return employee -> simplePassesFilter(employee.getFullName(), getFilterText())
                || simplePassesFilter(employee.getPosition(), getFilterText())
                || simplePassesFilter(employee.getPosition().getDescription(), getFilterText());
    }

}
