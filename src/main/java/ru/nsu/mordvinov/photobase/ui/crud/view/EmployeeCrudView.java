package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.EmployeeRepository;
import ru.nsu.mordvinov.photobase.backend.repository.KioskRepository;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.EmployeeForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.EmployeeGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.EmployeeCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.EmployeeDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "Employee", layout = MainLayout.class)
@PageTitle("Employee")
public class EmployeeCrudView extends BaseCrudView<Employee> {

    public static final String VIEW_NAME = "Employee";


    private final EmployeeCrudLogic viewLogic;
    private final EmployeeRepository employeeRepository;

    private final OfficeRepository officeRepository;
    private final BranchRepository branchRepository;
    private final KioskRepository kioskRepository;

    @Autowired
    public EmployeeCrudView(EmployeeRepository employeeRepository,
                            OfficeRepository officeRepository, BranchRepository branchRepository, KioskRepository kioskRepository) {
        this.employeeRepository = employeeRepository;
        this.officeRepository = officeRepository;
        this.branchRepository = branchRepository;
        this.kioskRepository = kioskRepository;
        this.viewLogic = new EmployeeCrudLogic(this,
                employeeRepository, this.officeRepository, branchRepository, this.kioskRepository);
        this.setRepository(employeeRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new EmployeeDataProvider(employeeRepository));
        this.setGrid(new EmployeeGrid());
        this.setForm(new EmployeeForm(viewLogic));
        this.setEntityName("employee");

        this.init();
        viewLogic.init();
    }


}
