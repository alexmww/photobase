package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.BranchCrudLogic;

public class BranchForm extends BaseForm<Branch> {

    private TextField name;
    private TextField address;
    private ComboBox<Office> office;

    public BranchForm(BranchCrudLogic viewLogic) {
        super(viewLogic, Branch.class, Branch::new, Branch::getId);

        name = new TextField("Branch name");
        name.setWidth("100%");
        name.setRequired(true);
        name.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(name);

        address = new TextField("Branch address");
        address.setWidth("100%");
        address.setRequired(true);
        address.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(address);

        office = new ComboBox<>("Office");
        office.setWidth("100%");
        office.setRequired(true);
        office.setItemLabelGenerator(Office::gridFormatted);
        office.setItems(viewLogic.getOfficeList());

        getContent().add(office);

        bindFields();
    }

}
