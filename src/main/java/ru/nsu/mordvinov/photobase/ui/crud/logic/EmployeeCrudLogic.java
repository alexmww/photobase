package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.EmployeeRepository;
import ru.nsu.mordvinov.photobase.backend.repository.KioskRepository;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.EmployeeCrudView;

import java.util.List;

@Slf4j
public class EmployeeCrudLogic extends BaseCrudLogic<Employee> {

    private EmployeeCrudView view;

    private final EmployeeRepository employeeRepository;

    private final OfficeRepository officeRepository;
    private final BranchRepository branchRepository;
    private final KioskRepository kioskRepository;

    public EmployeeCrudLogic(EmployeeCrudView kioskCrudView,
                             EmployeeRepository employeeRepository,
                             OfficeRepository officeRepository,
                             BranchRepository branchRepository,
                             KioskRepository kioskRepository) {
        super(kioskCrudView,
                EmployeeCrudView.class,
                employeeRepository,
                Employee::getId,
                Employee::new,
                Employee::getFullName);
        view = kioskCrudView;
        this.employeeRepository = employeeRepository;
        this.officeRepository = officeRepository;
        this.branchRepository = branchRepository;
        this.kioskRepository = kioskRepository;
    }

    public List<Office> getOfficeList() {
        return officeRepository.findAll();
    }

    public List<Branch> getBranchList() {
        return branchRepository.findAll();
    }

    public List<Kiosk> getKioskList() {
        return kioskRepository.findAll();
    }
}
