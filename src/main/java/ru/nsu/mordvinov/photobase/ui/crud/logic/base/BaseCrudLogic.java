package ru.nsu.mordvinov.photobase.ui.crud.logic.base;

import com.vaadin.flow.component.UI;
import ru.nsu.mordvinov.photobase.backend.model.BaseEntity;
import ru.nsu.mordvinov.photobase.backend.repository.BaseRepository;
import ru.nsu.mordvinov.photobase.ui.authentication.AccessControl;
import ru.nsu.mordvinov.photobase.ui.authentication.AccessControlFactory;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

import java.io.Serializable;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This class provides an interface for the logical operations between the CRUD
 * view, its parts like the entity editor form and the data source, including
 * fetching and saving entities.
 * <p>
 * Having this separate from the view makes it easier to test various parts of
 * the system separately, and to e.g. provide alternative views for the same
 * data.
 */

public abstract class BaseCrudLogic<T extends BaseEntity> implements Serializable {

    private final BaseCrudView<T> view;

    private final BaseRepository<T> repository;

    private final Function<T, Integer> idProvider;

    private final Supplier<T> entityProvider;

    private final Function<T, String> notificationNameProvider;

    private final Class viewClass;

    public BaseCrudLogic(BaseCrudView<T> simpleCrudView,
                         Class<? extends BaseCrudView<T>> viewClass,
                         BaseRepository<T> repository,
                         Function<T, Integer> idProvider,
                         Supplier<T> entityProvider,
                         Function<T, String> notificationNameProvider) {
        this.view = simpleCrudView;
        this.repository = repository;
        this.idProvider = idProvider;
        this.entityProvider = entityProvider;
        this.viewClass = viewClass;
        this.notificationNameProvider = notificationNameProvider;
    }

    public void init() {
        editEntity(null);
        // Hide and disable if not admin
        if (!AccessControlFactory.getInstance().createAccessControl()
                .isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
            view.setNewEntityEnabled(false);
        }
    }

    public void cancelEntity() {
        setFragmentParameter("");
        view.clearSelection();
    }

    /**
     * Update the fragment without causing navigator to change view
     */
    private void setFragmentParameter(String entityId) {
        String fragmentParameter;
        if (entityId == null || entityId.isEmpty()) {
            fragmentParameter = "";
        } else {
            fragmentParameter = entityId;
        }

        try {
            UI.getCurrent().navigate(viewClass, fragmentParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enter(String entityId) {
        if (entityId != null && !entityId.isEmpty()) {
            if (entityId.equals("new")) {
                newEntity();
            } else {
                // Ensure this is selected even if coming directly here from
                // login
                try {
                    int pid = Integer.parseInt(entityId);
                    T entity = findEntity(pid);
                    view.selectRow(entity);
                } catch (NumberFormatException ignore) {
                }
            }
        } else {
            view.showForm(false);
        }
    }

    private T findEntity(int entityId) {
        return repository.getOne(entityId);
    }

    public void saveEntity(T entity) {
        boolean newEntity = idProvider.apply(entity) == null;
        view.clearSelection();
        try {
            view.updateEntity(entity);
            setFragmentParameter("");
            if (newEntity) {
                UI.getCurrent().getPage().reload(); // TODO из-за ошибки с Lazy и роутингом, приходится перезагружать страницу
            }
            showNotification(notificationNameProvider.apply(entity) + (newEntity ? " created" : " updated"));
        } catch (Exception e) {
            showNotification("Could not save entity. Try to reload page.");
        }

    }

    public void deleteEntity(T entity) {
        try {
            view.clearSelection();
            view.removeEntity(entity);
            setFragmentParameter("");
            UI.getCurrent().getPage().reload(); // TODO из-за ошибки с Lazy и роутингом, приходится перезагружать страницу
            showNotification(notificationNameProvider.apply(entity) + " removed");
        } catch (Exception e) {
            showNotification("Could not delete entity. Try to delete dependent entities first.");
        }
    }

    public void editEntity(T entity) {
        if (entity == null) {
            setFragmentParameter("");
        } else {
//            setFragmentParameter(entity.getId() + ""); //TODO fix routing
        }
        try {
            view.editEntity(entity);
        } catch (Exception e) {
            showNotification("An error occurred  while editing entity. Try to reload page.");
        }
    }

    public void newEntity() {
        view.clearSelection();
        setFragmentParameter("new");
        try {
            view.editEntity(entityProvider.get());
        } catch (Exception e) {
            showNotification("Unexpected error.");
        }
    }

    public void rowSelected(T entity) {
        if (AccessControlFactory.getInstance().createAccessControl()
                .isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
            editEntity(entity);
        }
    }

    public void showNotification(String message) {
        view.showNotification(message);
    }
}
