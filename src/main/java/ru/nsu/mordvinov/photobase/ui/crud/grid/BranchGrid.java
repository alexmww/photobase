package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;

public class BranchGrid extends BaseGrid<Branch> {

    public BranchGrid() {
        super();

        addColumn(Branch::getName)
                .setHeader("Branch name")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(Branch::getAddress)
                .setHeader("Branch address")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(b -> b.getOffice().gridFormatted())
                .setHeader("Parent office")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
