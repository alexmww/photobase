package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.BranchCrudView;

import java.util.List;

@Slf4j
public class BranchCrudLogic extends BaseCrudLogic<Branch> {

    private BranchCrudView view;

    private final BranchRepository branchRepository;
    private final OfficeRepository officeRepository;

    public BranchCrudLogic(BranchCrudView branchCrudView,
                           BranchRepository branchRepository,
                           OfficeRepository officeRepository) {
        super(branchCrudView,
                BranchCrudView.class,
                branchRepository,
                Branch::getId,
                Branch::new,
                Branch::getName);
        view = branchCrudView;
        this.branchRepository = branchRepository;
        this.officeRepository = officeRepository;
    }

    public List<Office> getOfficeList() {
        return officeRepository.findAll();
    }
}
