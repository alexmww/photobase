package ru.nsu.mordvinov.photobase.backend.model;

import lombok.Data;
import lombok.ToString;
import ru.nsu.mordvinov.photobase.backend.model.enums.Position;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@Table(name = "employee")
public class Employee implements BaseEntity {

    @ToString.Exclude
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(min = 3, message = "Employee name must have at least three characters")
    @Size(max = 100, message = "Employee name must have less than 100 characters")
    private String fullName;

    @NotNull(message = "Position can not be empty")
    @Column(name = "position")
    @Enumerated(EnumType.STRING)
    private Position position;

    @ManyToOne
    @JoinColumn(name="office_id")
    private Office office;

    @ManyToOne
    @JoinColumn(name="branch_id")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name="kiosk_id")
    private Kiosk kiosk;

    @ToString.Exclude
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<PrintOrder> printOrders;

    public String gridFormatted() {
        return fullName;
    }
}
