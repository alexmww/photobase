package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.model.enums.Position;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.EmployeeCrudLogic;

public class EmployeeForm extends BaseForm<Employee> {

    private TextField fullName;
    private ComboBox<Position> position;

    private ComboBox<Office> office;
    private ComboBox<Branch> branch;
    private ComboBox<Kiosk> kiosk;

    private EmployeeCrudLogic viewLogic;

    public EmployeeForm(EmployeeCrudLogic viewLogic) {
        super(viewLogic, Employee.class, Employee::new, Employee::getId);
        this.viewLogic = viewLogic;

        fullName = new TextField("Full name");
        fullName.setWidth("100%");
        fullName.setRequired(true);
        fullName.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(fullName);

        position = new ComboBox<>("Position");
        position.setWidth("100%");
        position.setRequired(true);
        position.setItems(Position.values());
        position.setItemLabelGenerator(Position::getDescription);
        getContent().add(position);

        office = new ComboBox<>("Office");
        office.setWidth("100%");
        office.setRequired(true);
        office.setItems(viewLogic.getOfficeList());
        office.setItemLabelGenerator(Office::gridFormatted);
        getContent().add(office);

        branch = new ComboBox<>("Branch");
        branch.setWidth("100%");
        branch.setRequired(true);
        branch.setItems(viewLogic.getBranchList());
        branch.setItemLabelGenerator(Branch::gridFormatted);
        getContent().add(branch);

        kiosk = new ComboBox<>("Kiosk");
        kiosk.setWidth("100%");
        kiosk.setRequired(true);
        kiosk.setItems(viewLogic.getKioskList());
        kiosk.setItemLabelGenerator(Kiosk::gridFormatted);
        getContent().add(kiosk);

        bindFields();
    }


    @Override
    protected boolean customValidation() {
        boolean res = office.getValue() != null ||
                branch.getValue() != null ||
                kiosk.getValue() != null;
        if (!res) {
            viewLogic.showNotification("Choose al least one Office, Branch or Kiosk.");
        }
        return res;
    }
}
