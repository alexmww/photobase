package ru.nsu.mordvinov.photobase.ui.crud.grid.base;

import com.vaadin.flow.component.grid.Grid;
import ru.nsu.mordvinov.photobase.backend.model.BaseEntity;

/**
 * Grid of entities, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public abstract class BaseGrid<T extends BaseEntity> extends Grid<T> {

    public BaseGrid() {
        setSizeFull();
    }

    public T getSelectedRow() {
        return asSingleSelect().getValue();
    }

    public void refresh(T entity) {
        getDataCommunicator().refresh(entity);
    }
}
