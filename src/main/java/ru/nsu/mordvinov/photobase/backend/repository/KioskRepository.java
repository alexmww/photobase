package ru.nsu.mordvinov.photobase.backend.repository;

import org.springframework.stereotype.Repository;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;


@Repository
public interface KioskRepository extends BaseRepository<Kiosk> {
}