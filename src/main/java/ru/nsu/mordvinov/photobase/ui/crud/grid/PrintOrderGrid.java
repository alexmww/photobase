package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;
import ru.nsu.mordvinov.photobase.ui.utils.DateFormatter;

public class PrintOrderGrid extends BaseGrid<PrintOrder> {

    public PrintOrderGrid() {
        super();

        addColumn(PrintOrder::getId)
                .setHeader("Order id")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(e -> e.getClient().gridFormatted())
                .setHeader("Client")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(e -> e.getEmployee().gridFormatted())
                .setHeader("Employee")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(PrintOrder::getComment)
                .setHeader("Comment")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(e -> DateFormatter.format(e.getDate()))
                .setHeader("Date")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(PrintOrder::getIsDone)
                .setHeader("Done")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
