package ru.nsu.mordvinov.photobase.ui.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatter {

    public static String format(LocalDateTime l) {
        if (l == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return l.format(formatter);
    }
}
