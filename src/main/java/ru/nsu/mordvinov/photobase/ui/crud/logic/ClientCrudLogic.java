package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.backend.repository.ClientRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.ClientCrudView;

@Slf4j
public class ClientCrudLogic extends BaseCrudLogic<Client> {

    private ClientCrudView view;

    private final ClientRepository clientRepository;

    public ClientCrudLogic(ClientCrudView simpleCrudView,
                           ClientRepository clientRepository) {
        super(simpleCrudView, ClientCrudView.class, clientRepository, Client::getId, Client::new, Client::getFullName);
        view = simpleCrudView;
        this.clientRepository = clientRepository;
    }

    @Override
    public void saveEntity(Client entity) {
        super.saveEntity(entity);
    }
}
