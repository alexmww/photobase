package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;

public class OfficeGrid extends BaseGrid<Office> {

    public OfficeGrid() {
        super();

        addColumn(Office::getName)
                .setHeader("Office name")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(Office::getAddress)
                .setHeader("Office address")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
