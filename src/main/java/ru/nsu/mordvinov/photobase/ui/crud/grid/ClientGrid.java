package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;

public class ClientGrid extends BaseGrid<Client> {

    public ClientGrid() {
        super();

        addColumn(Client::getFullName)
                .setHeader("Client name")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(Client::getPhone)
                .setHeader("Phone")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(Client::getEmail)
                .setHeader("Email")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
