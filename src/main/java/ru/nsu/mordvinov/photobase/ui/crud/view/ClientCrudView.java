package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.backend.repository.ClientRepository;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.ClientForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.ClientGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.ClientCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.ClientDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "Client", layout = MainLayout.class)
@PageTitle("Client")
public class ClientCrudView extends BaseCrudView<Client> {

    public static final String VIEW_NAME = "Client";


    private final ClientCrudLogic viewLogic;
    private final ClientRepository clientRepository;
    private final OfficeRepository officeRepository;

    @Autowired
    public ClientCrudView(ClientRepository clientRepository,
                          OfficeRepository officeRepository) {
        this.clientRepository = clientRepository;
        this.officeRepository = officeRepository;
        this.viewLogic = new ClientCrudLogic(this, clientRepository);
        this.setRepository(clientRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new ClientDataProvider(clientRepository));
        this.setGrid(new ClientGrid());
        this.setForm(new ClientForm(viewLogic));
        this.setEntityName("client");

        this.init();
        viewLogic.init();
    }


}
