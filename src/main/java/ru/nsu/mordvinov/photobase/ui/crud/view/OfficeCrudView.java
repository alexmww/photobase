package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.OfficeForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.OfficeGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.OfficeCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.OfficeDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "Office", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
@PageTitle("Office")
public class OfficeCrudView extends BaseCrudView<Office> {

    public static final String VIEW_NAME = "Office";


    private final OfficeCrudLogic viewLogic;
    private final OfficeRepository officeRepository;

    @Autowired
    public OfficeCrudView(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
        this.viewLogic = new OfficeCrudLogic(this, officeRepository);
        this.setRepository(officeRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new OfficeDataProvider(officeRepository));
        this.setGrid(new OfficeGrid());
        this.setForm(new OfficeForm(viewLogic));
        this.setEntityName("office");

        this.init();
        viewLogic.init();
    }


}
