package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.OfficeCrudView;

@Slf4j
public class OfficeCrudLogic extends BaseCrudLogic<Office> {

    private OfficeCrudView view;

    private final OfficeRepository officeRepository;

    public OfficeCrudLogic(OfficeCrudView simpleCrudView,
                           OfficeRepository officeRepository) {
        super(simpleCrudView, OfficeCrudView.class, officeRepository, Office::getId, Office::new, Office::getName);
        view = simpleCrudView;
        this.officeRepository = officeRepository;
    }

    @Override
    public void saveEntity(Office entity) {
        super.saveEntity(entity);
    }
}
