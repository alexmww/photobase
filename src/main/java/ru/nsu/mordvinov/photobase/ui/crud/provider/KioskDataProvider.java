package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.repository.KioskRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class KioskDataProvider extends BaseProvider<Kiosk> {


    public KioskDataProvider(KioskRepository kioskRepository) {
        super(kioskRepository, Kiosk::getId);
    }

    @Override
    protected SerializablePredicate<Kiosk> filter() {
        return kiosk -> simplePassesFilter(kiosk.getName(), getFilterText())
                || simplePassesFilter(kiosk.getAddress(), getFilterText());
    }

}
