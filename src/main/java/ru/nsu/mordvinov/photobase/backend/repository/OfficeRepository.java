package ru.nsu.mordvinov.photobase.backend.repository;

import org.springframework.stereotype.Repository;
import ru.nsu.mordvinov.photobase.backend.model.Office;


@Repository
public interface OfficeRepository extends BaseRepository<Office> {
}