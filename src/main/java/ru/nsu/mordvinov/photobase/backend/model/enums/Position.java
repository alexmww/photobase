package ru.nsu.mordvinov.photobase.backend.model.enums;

import lombok.Getter;

public enum Position {
    MANAGER("Manager"),
    PRINT_WORKER("Print worker");

    @Getter
    private String description;

    Position(String description) {
        this.description = description;
    }
}
