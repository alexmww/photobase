package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.KioskCrudLogic;

public class KioskForm extends BaseForm<Kiosk> {

    private TextField name;
    private TextField address;
    private ComboBox<Branch> branch;

    public KioskForm(KioskCrudLogic viewLogic) {
        super(viewLogic, Kiosk.class, Kiosk::new, Kiosk::getId);

        name = new TextField("Kiosk name");
        name.setWidth("100%");
        name.setRequired(true);
        name.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(name);

        address = new TextField("Kiosk address");
        address.setWidth("100%");
        address.setRequired(true);
        address.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(address);

        branch = new ComboBox<>("Branch");
        branch.setWidth("100%");
        branch.setRequired(true);
        branch.setItemLabelGenerator(Branch::gridFormatted);
        branch.setItems(viewLogic.getBranchList());

        getContent().add(branch);

        bindFields();
    }

}
