package ru.nsu.mordvinov.photobase.backend.model.enums;

import lombok.Getter;

import java.io.Serializable;

public enum PaperType implements Serializable {
    GLOSSY("Глянцевая бумага"),
    MATTE("Матовая бумага");

    @Getter
    private String description;

    PaperType(String description) {
        this.description = description;
    }
}
