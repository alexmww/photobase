package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.Employee;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;

public class EmployeeGrid extends BaseGrid<Employee> {

    public EmployeeGrid() {
        super();

        addColumn(Employee::getFullName)
                .setHeader("Employee name")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(e -> e.getPosition().getDescription())
                .setHeader("Position")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(employee ->
                employee.getOffice() == null ? "" : employee.getOffice().gridFormatted())
                .setHeader("Office")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(employee ->
                employee.getBranch() == null ? "" : employee.getBranch().gridFormatted())
                .setHeader("Branch")
                .setFlexGrow(20)
                .setSortable(true);


        addColumn(employee ->
                employee.getKiosk() == null ? "" : employee.getKiosk().gridFormatted())
                .setHeader("Kiosk")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
