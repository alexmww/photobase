package ru.nsu.mordvinov.photobase.ui.crud.logic;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.KioskRepository;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.view.KioskCrudView;

import java.util.List;

@Slf4j
public class KioskCrudLogic extends BaseCrudLogic<Kiosk> {

    private KioskCrudView view;

    private final KioskRepository kioskRepository;
    private final BranchRepository branchRepository;

    public KioskCrudLogic(KioskCrudView kioskCrudView,
                          KioskRepository kioskRepository,
                          BranchRepository branchRepository) {
        super(kioskCrudView,
                KioskCrudView.class,
                kioskRepository,
                Kiosk::getId,
                Kiosk::new,
                Kiosk::getName);
        view = kioskCrudView;
        this.kioskRepository = kioskRepository;
        this.branchRepository = branchRepository;
    }

    public List<Branch> getBranchList() {
        return branchRepository.findAll();
    }
}
