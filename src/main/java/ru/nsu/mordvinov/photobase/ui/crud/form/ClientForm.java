package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.ClientCrudLogic;

public class ClientForm extends BaseForm<Client> {


    private final TextField fullName;
    private final TextField phone;
    private final TextField email;


    private ClientCrudLogic viewLogic;

    public ClientForm(ClientCrudLogic viewLogic) {
        super(viewLogic, Client.class, Client::new, Client::getId);
        this.viewLogic = viewLogic;

        fullName = new TextField("Full name");
        fullName.setWidth("100%");
        fullName.setRequired(true);
        fullName.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(fullName);

        phone = new TextField("Phone");
        phone.setWidth("100%");
        phone.setRequired(true);
        phone.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(phone);

        email = new TextField("Email");
        email.setWidth("100%");
        email.setRequired(true);
        email.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(email);

        getBinder().forField(phone)
                .withValidator(new PhoneValidation("Phone number must be in format: +7**********"))
                .bind("phone");

        getBinder().forField(email)
                .withValidator(new EmailValidator("Expected valid email address"))
                .bind("email");

        bindFields();
    }

    private class PhoneValidation extends RegexpValidator {

        private static final String phoneRegex = "\\+[7][0-9]{10}";

        PhoneValidation(String errorMessage) {
            super(errorMessage, phoneRegex);
        }
    }
}
