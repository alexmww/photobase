package ru.nsu.mordvinov.photobase.backend.repository;

import org.springframework.stereotype.Repository;
import ru.nsu.mordvinov.photobase.backend.model.Employee;


@Repository
public interface EmployeeRepository extends BaseRepository<Employee> {
}