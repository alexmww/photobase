package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;
import ru.nsu.mordvinov.photobase.backend.repository.PrintOrderRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class PrintOrderDataProvider extends BaseProvider<PrintOrder> {


    public PrintOrderDataProvider(PrintOrderRepository printOrderRepository) {
        super(printOrderRepository, PrintOrder::getId);
    }

    @Override
    protected SerializablePredicate<PrintOrder> filter() {
        return printOrder -> simplePassesFilter(printOrder.getComment(), getFilterText())
                || simplePassesFilter(printOrder.getClient().getFullName(), getFilterText())
                || simplePassesFilter(printOrder.getEmployee().getFullName(), getFilterText());
    }

}
