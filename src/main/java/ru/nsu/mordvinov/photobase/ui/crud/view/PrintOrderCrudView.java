package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.PrintOrder;
import ru.nsu.mordvinov.photobase.backend.repository.ClientRepository;
import ru.nsu.mordvinov.photobase.backend.repository.EmployeeRepository;
import ru.nsu.mordvinov.photobase.backend.repository.PrintOrderRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.PrintOrderForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.PrintOrderGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.PrintOrderCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.PrintOrderDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "PrintOrder", layout = MainLayout.class)
@PageTitle("PrintOrder")
public class PrintOrderCrudView extends BaseCrudView<PrintOrder> {

    public static final String VIEW_NAME = "PrintOrder";


    private final PrintOrderCrudLogic viewLogic;
    private final PrintOrderRepository printOrderRepository;
    private final ClientRepository clientRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public PrintOrderCrudView(PrintOrderRepository printOrderRepository,
                              ClientRepository clientRepository,
                              EmployeeRepository employeeRepository) {
        this.printOrderRepository = printOrderRepository;
        this.viewLogic = new PrintOrderCrudLogic(this,
                printOrderRepository, clientRepository, employeeRepository);
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.setRepository(printOrderRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new PrintOrderDataProvider(printOrderRepository));
        this.setGrid(new PrintOrderGrid());
        this.setForm(new PrintOrderForm(viewLogic));
        this.setEntityName("order");

        this.init();
        viewLogic.init();
    }


}
