package ru.nsu.mordvinov.photobase.ui.crud.grid;

import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;

public class KioskGrid extends BaseGrid<Kiosk> {

    public KioskGrid() {
        super();

        addColumn(Kiosk::getName)
                .setHeader("Kiosk name")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(Kiosk::getAddress)
                .setHeader("Kiosk address")
                .setFlexGrow(20)
                .setSortable(true);

        addColumn(k -> k.getBranch().gridFormatted())
                .setHeader("Parent branch")
                .setFlexGrow(20)
                .setSortable(true);

    }

}
