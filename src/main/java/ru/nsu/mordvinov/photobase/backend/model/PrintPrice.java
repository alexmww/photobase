package ru.nsu.mordvinov.photobase.backend.model;

import lombok.Data;
import ru.nsu.mordvinov.photobase.backend.model.enums.PaperType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "print_price")
public class PrintPrice implements BaseEntity, Serializable {

    @Id
    @Column(name = "paper_type")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private PaperType paperType;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "price")
    private BigDecimal price;

}
