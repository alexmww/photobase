package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.BranchForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.BranchGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.BranchCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.BranchDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "Branch", layout = MainLayout.class)
@PageTitle("Branch")
public class BranchCrudView extends BaseCrudView<Branch> {

    public static final String VIEW_NAME = "Branch";


    private final BranchCrudLogic viewLogic;
    private final BranchRepository branchRepository;
    private final OfficeRepository officeRepository;

    @Autowired
    public BranchCrudView(BranchRepository branchRepository,
                          OfficeRepository officeRepository) {
        this.branchRepository = branchRepository;
        this.officeRepository = officeRepository;
        this.viewLogic = new BranchCrudLogic(this, branchRepository, officeRepository);
        this.setRepository(branchRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new BranchDataProvider(branchRepository));
        this.setGrid(new BranchGrid());
        this.setForm(new BranchForm(viewLogic));
        this.setEntityName("branch");

        this.init();
        viewLogic.init();
    }


}
