package ru.nsu.mordvinov.photobase.ui.crud.view.base;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import lombok.AccessLevel;
import lombok.Setter;
import ru.nsu.mordvinov.photobase.backend.model.BaseEntity;
import ru.nsu.mordvinov.photobase.backend.repository.BaseRepository;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.base.BaseGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.OfficeCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;

/**
 * A view for performing create-read-update-delete operations on entities.
 * <p>
 * See also {@link OfficeCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public abstract class BaseCrudView<T extends BaseEntity> extends HorizontalLayout
        implements HasUrlParameter<String> {

    private static final int DURATION = 6000;

    @Setter(AccessLevel.PROTECTED)
    private BaseGrid<T> grid;
    @Setter(AccessLevel.PROTECTED)
    private BaseForm<T> form;
    @Setter(AccessLevel.PROTECTED)
    private BaseCrudLogic<T> viewLogic;
    @Setter(AccessLevel.PROTECTED)
    private BaseRepository<T> repository;
    @Setter(AccessLevel.PROTECTED)
    private BaseProvider<T> dataProvider;
    @Setter(AccessLevel.PROTECTED)
    private String entityName;

    private TextField filter;
    private Button newEntity;


    public BaseCrudView() {
    }

    public HorizontalLayout createTopBar() {
        filter = new TextField();
        filter.setPlaceholder("Filter...");
        // Apply the filter to grid's data provider. TextField value is never null
        filter.addValueChangeListener(event -> dataProvider.setFilter(event.getValue()));

        newEntity = new Button(String.format("new %s", entityName));
        newEntity.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        newEntity.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        newEntity.addClickListener(click -> viewLogic.newEntity());
        // CTRL+N will create a new window which is unavoidable

        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setWidth("100%");
        topLayout.add(filter);
        topLayout.add(newEntity);
        topLayout.setVerticalComponentAlignment(Alignment.START, filter);
        topLayout.expand(filter);
        return topLayout;
    }

    protected void init() {
        setSizeFull();
        HorizontalLayout topLayout = createTopBar();

        grid.setDataProvider(dataProvider);
        grid.asSingleSelect().addValueChangeListener(
                event -> viewLogic.rowSelected(event.getValue()));


        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.add(topLayout);
        barAndGridLayout.add(grid);
        barAndGridLayout.setFlexGrow(1, grid);
        barAndGridLayout.setFlexGrow(0, topLayout);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.expand(grid);

        add(barAndGridLayout);
        add(form);

        viewLogic.init();
    }

    public void showNotification(String msg) {
        Notification.show(msg, DURATION, Notification.Position.TOP_CENTER);
    }

    public void setNewEntityEnabled(boolean enabled) {
        newEntity.setEnabled(enabled);
    }

    public void clearSelection() {
        grid.getSelectionModel().deselectAll();
    }

    public void selectRow(T row) {
        grid.getSelectionModel().select(row);
    }

    public T getSelectedRow() {
        return grid.getSelectedRow();
    }

    public void updateEntity(T entity) {
        dataProvider.save(entity);
        //TODO catch here?
    }

    public void removeEntity(T entity) {
        dataProvider.delete(entity);
    }

    public void editEntity(T entity) {
        showForm(entity != null);
        form.editEntity(entity);
    }

    public void showForm(boolean show) {
        form.setVisible(show);
    }

    @Override
    public void setParameter(BeforeEvent event,
                             @OptionalParameter String parameter) {
        viewLogic.enter(parameter);
    }
}
