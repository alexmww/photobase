package ru.nsu.mordvinov.photobase.ui.crud.provider.base;

import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.function.SerializablePredicate;
import lombok.Getter;
import ru.nsu.mordvinov.photobase.backend.model.BaseEntity;
import ru.nsu.mordvinov.photobase.backend.repository.BaseRepository;

import java.util.Objects;
import java.util.function.Function;

public abstract class BaseProvider<T extends BaseEntity> extends ListDataProvider<T> {

    /**
     * Text filter that can be changed separately.
     */
    @Getter
    private String filterText = "";

    private final Function<T, Integer> idProvider;

    private final BaseRepository<T> repository;

    public BaseProvider(BaseRepository<T> repository, Function<T, Integer> idProvider) {
        super(repository.findAll());
        this.idProvider = idProvider;
        this.repository = repository;
    }

    /**
     * Store given entity to it's repository.
     *
     * @param entity the updated or new entity
     */
    public void save(T entity) {
        boolean newEntity = idProvider.apply(entity) == null;

        repository.save(entity);
        if (newEntity) {
            refreshAll();
        } else {
            refreshItem(entity);
        }
    }

    /**
     * Delete given entity it's repository.
     *
     * @param entity the entity to be deleted
     */
    public void delete(T entity) {
        repository.delete(entity);
        refreshAll();
    }

    /**
     * Sets the filter to use for this data provider and refreshes data.
     * <p>
     * {@link #filter() } lambda is applied.
     *
     * @param filterText the text to filter by, never null
     */
    public void setFilter(String filterText) {
        Objects.requireNonNull(filterText, "Filter text cannot be null.");
        String formatted = filterText.trim().toLowerCase();

        if (Objects.equals(this.filterText, formatted)) {
            return;
        }
        this.filterText = formatted;

        setFilter(filter());
    }

    /**
     * Abstract filter
     *
     * @return predicate
     */
    protected abstract SerializablePredicate<T> filter();

    @Override
    public Integer getId(T entity) {
        Objects.requireNonNull(entity, "Cannot provide an id for a null entity.");

        return idProvider.apply(entity);
    }

    protected boolean simplePassesFilter(Object object, String filterText) {
        return object != null &&
                object.toString().toLowerCase().contains(filterText);
    }
}
