package ru.nsu.mordvinov.photobase.backend.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@Table(name = "kiosk")
public class Kiosk implements BaseEntity {

    @ToString.Exclude
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(min = 3, message = "Kiosk name must have at least three characters")
    @Size(max = 100, message = "Kiosk name must have less than 100 characters")
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 3, message = "Kiosk address must have at least three characters")
    @Size(max = 100, message = "Kiosk address must have less than 100 characters")
    @Column(name = "address")
    private String address;

    @NotNull(message = "Branch is required")
    @ManyToOne
    @JoinColumn(name="branch_id", nullable=false)
    private Branch branch;

    @ToString.Exclude
    @OneToMany(mappedBy="kiosk", fetch = FetchType.LAZY)
    private List<Employee> employees;

    public String gridFormatted() {
        return name + ", " + address;
    }
}
