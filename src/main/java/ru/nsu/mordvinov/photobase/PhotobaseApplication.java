package ru.nsu.mordvinov.photobase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotobaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotobaseApplication.class, args);
	}

}
