package ru.nsu.mordvinov.photobase.ui;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.RouteBaseData;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.Command;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import ru.nsu.mordvinov.photobase.ui.about.AboutView;
import ru.nsu.mordvinov.photobase.ui.crud.view.BranchCrudView;
import ru.nsu.mordvinov.photobase.ui.crud.view.ClientCrudView;
import ru.nsu.mordvinov.photobase.ui.crud.view.EmployeeCrudView;
import ru.nsu.mordvinov.photobase.ui.crud.view.KioskCrudView;
import ru.nsu.mordvinov.photobase.ui.crud.view.OfficeCrudView;
import ru.nsu.mordvinov.photobase.ui.crud.view.PrintOrderCrudView;

/**
 * The main layout. Contains the navigation menu.
 */
@HtmlImport("css/shared-styles.html")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class MainLayout extends FlexLayout implements RouterLayout {
    private Menu menu;

    public MainLayout() {
        setSizeFull();
        setClassName("main-layout");

        menu = new Menu();
        menu.addView(OfficeCrudView.class, OfficeCrudView.VIEW_NAME,
                VaadinIcon.OFFICE.create());
        menu.addView(BranchCrudView.class, BranchCrudView.VIEW_NAME,
                VaadinIcon.SHOP.create());
        menu.addView(KioskCrudView.class, KioskCrudView.VIEW_NAME,
                VaadinIcon.STORAGE.create());
        menu.addView(EmployeeCrudView.class, EmployeeCrudView.VIEW_NAME,
                VaadinIcon.SPECIALIST.create());
        menu.addView(ClientCrudView.class, ClientCrudView.VIEW_NAME,
                VaadinIcon.USER.create());
        menu.addView(PrintOrderCrudView.class, PrintOrderCrudView.VIEW_NAME,
                VaadinIcon.CART.create());
        menu.addView(AboutView.class, AboutView.VIEW_NAME,
                VaadinIcon.INFO_CIRCLE.create());

        add(menu);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);

        // add the admin view menu item if/when it is registered dynamically
        Command addAdminMenuItemCommand = () -> menu.addView(AdminView.class,
                AdminView.VIEW_NAME, VaadinIcon.DOCTOR.create());
        RouteConfiguration sessionScopedConfiguration = RouteConfiguration
                .forSessionScope();
        if (sessionScopedConfiguration.isRouteRegistered(AdminView.class)) {
            addAdminMenuItemCommand.execute();
        } else {
            sessionScopedConfiguration.addRoutesChangeListener(event -> {
                for (RouteBaseData data : event.getAddedRoutes()) {
                    if (data.getNavigationTarget().equals(AdminView.class)) {
                        addAdminMenuItemCommand.execute();
                    }
                }
            });
        }
    }
}
