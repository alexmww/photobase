package ru.nsu.mordvinov.photobase.backend.repository;

import org.springframework.stereotype.Repository;
import ru.nsu.mordvinov.photobase.backend.model.Branch;


@Repository
public interface BranchRepository extends BaseRepository<Branch> {

    Branch findTopByOrderByIdDesc();
}