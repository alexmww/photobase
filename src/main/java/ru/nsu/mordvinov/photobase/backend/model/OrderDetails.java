package ru.nsu.mordvinov.photobase.backend.model;

import lombok.Data;
import ru.nsu.mordvinov.photobase.backend.model.enums.Format;
import ru.nsu.mordvinov.photobase.backend.model.enums.PaperType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "order_details")
public class OrderDetails implements BaseEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "paper_type")
    @Enumerated(EnumType.STRING)
    private PaperType paperType;

    @Column(name = "format")
    @Enumerated(EnumType.STRING)
    private Format format;

    @Column(name = "amount")
    private Integer amount;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private PrintOrder printOrder;
}
