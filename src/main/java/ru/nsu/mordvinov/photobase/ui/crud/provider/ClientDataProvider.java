package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.Client;
import ru.nsu.mordvinov.photobase.backend.repository.ClientRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class ClientDataProvider extends BaseProvider<Client> {


    public ClientDataProvider(ClientRepository clientRepository) {
        super(clientRepository, Client::getId);
    }

    @Override
    protected SerializablePredicate<Client> filter() {
        return client -> simplePassesFilter(client.getFullName(), getFilterText())
                || simplePassesFilter(client.getPhone(), getFilterText());
    }

}
