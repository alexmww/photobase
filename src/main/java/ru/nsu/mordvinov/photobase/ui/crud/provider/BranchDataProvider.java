package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class BranchDataProvider extends BaseProvider<Branch> {


    public BranchDataProvider(BranchRepository branchRepository) {
        super(branchRepository, Branch::getId);
    }

    @Override
    protected SerializablePredicate<Branch> filter() {
        return branch -> simplePassesFilter(branch.getName(), getFilterText())
                || simplePassesFilter(branch.getAddress(), getFilterText());
    }

}
