package ru.nsu.mordvinov.photobase.ui.crud.form;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.ui.crud.form.base.BaseForm;
import ru.nsu.mordvinov.photobase.ui.crud.logic.OfficeCrudLogic;

public class OfficeForm extends BaseForm<Office> {

    private TextField name;
    private TextField address;

    public OfficeForm(OfficeCrudLogic viewLogic) {
        super(viewLogic, Office.class, Office::new, Office::getId);

        name = new TextField("Office name");
        name.setWidth("100%");
        name.setRequired(true);
        name.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(name);

        address = new TextField("Office address");
        address.setWidth("100%");
        address.setRequired(true);
        address.setValueChangeMode(ValueChangeMode.EAGER);
        getContent().add(address);
        bindFields();
    }
}
