package ru.nsu.mordvinov.photobase.ui.crud.form.base;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import lombok.AccessLevel;
import lombok.Getter;
import ru.nsu.mordvinov.photobase.backend.model.BaseEntity;
import ru.nsu.mordvinov.photobase.ui.crud.logic.base.BaseCrudLogic;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A form for editing a single entity.
 */
public abstract class BaseForm<T extends BaseEntity> extends Div {

    @Getter(AccessLevel.PROTECTED)
    private VerticalLayout content;

    private VerticalLayout buttons;

    private Button save;
    private Button discard;
    private Button cancel;
    private Button delete;

    @Getter(AccessLevel.PROTECTED)
    private Binder<T> binder;

    @Getter(AccessLevel.PROTECTED)
    private T currentEntity;

    private final BaseCrudLogic<T> viewLogic;
    private final Supplier<T> entityProvider;
    private final Function<T, Integer> idProvider;


    public BaseForm(BaseCrudLogic<T> viewLogic,
                    Class<T> entityClass,
                    Supplier<T> entityProvider,
                    Function<T, Integer> idProvider) {
        this.viewLogic = viewLogic;
        this.entityProvider = entityProvider;
        this.idProvider = idProvider;

        setClassName("base-form");

        content = createVerticalLayout();
        add(content);

        buttons = createVerticalLayout();
        add(buttons);

        binder = new BeanValidationBinder<>(entityClass);

        // enable/disable save button while editing
        binder.addStatusChangeListener(event -> {
            boolean isValid = !event.hasValidationErrors();
            boolean hasChanges = binder.hasChanges();
            save.setEnabled(hasChanges && isValid);
            discard.setEnabled(hasChanges);
        });

        initButtons();
        buttons.add(save, discard, delete, cancel);
    }

    /**
     * Override this method to add custom validation logic before save
     *
     * @return true if valid, false otherwise
     */
    protected boolean customValidation() {
        return true;
    }

    /**
     * Override this method to add custom logic before save
     */
    protected void customBeforeSave() {
    }

    private void initButtons() {
        save = new Button("Save");
        save.setWidth("100%");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        save.addClickListener(event -> {
            if (currentEntity != null && customValidation() && binder.writeBeanIfValid(currentEntity)) {
                customBeforeSave();
                viewLogic.saveEntity(currentEntity);
            }
        });

        discard = new Button("Discard changes");
        discard.setWidth("100%");
        discard.addClickListener(event -> viewLogic.editEntity(currentEntity));

        cancel = new Button("Cancel");
        cancel.setWidth("100%");
        cancel.addClickListener(event -> viewLogic.cancelEntity());
        cancel.addClickShortcut(Key.ESCAPE);
        getElement()
                .addEventListener("keydown", event -> viewLogic.cancelEntity())
                .setFilter("event.key == 'Escape'");

        delete = new Button("Delete");
        delete.setWidth("100%");
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
        delete.addClickListener(event -> {
            if (currentEntity != null) {
                viewLogic.deleteEntity(currentEntity);
            }
        });
    }

    public void editEntity(T entity) {
        if (entity == null) {
            entity = entityProvider.get();
        }
        delete.setVisible(idProvider.apply(entity) != null);
        currentEntity = entity;
        binder.readBean(entity);
    }

    private VerticalLayout createVerticalLayout() {
        VerticalLayout tmp = new VerticalLayout();
        tmp.setSizeUndefined();
        return tmp;
    }


    /**
     * Binding fields.
     * Be sure to call this method in the heir class.
     */
    protected final void bindFields() {
        binder.bindInstanceFields(this);
    }

}
