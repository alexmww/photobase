package ru.nsu.mordvinov.photobase.ui.crud.provider;

import com.vaadin.flow.function.SerializablePredicate;
import ru.nsu.mordvinov.photobase.backend.model.Office;
import ru.nsu.mordvinov.photobase.backend.repository.OfficeRepository;
import ru.nsu.mordvinov.photobase.ui.crud.provider.base.BaseProvider;


public class OfficeDataProvider extends BaseProvider<Office> {


    public OfficeDataProvider(OfficeRepository officeRepository) {
        super(officeRepository, Office::getId);
    }

    @Override
    protected SerializablePredicate<Office> filter() {
        return office -> simplePassesFilter(office.getName(), getFilterText())
                || simplePassesFilter(office.getAddress(), getFilterText());
    }

}
