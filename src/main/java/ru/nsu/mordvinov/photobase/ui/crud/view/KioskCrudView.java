package ru.nsu.mordvinov.photobase.ui.crud.view;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.repository.BranchRepository;
import ru.nsu.mordvinov.photobase.backend.repository.KioskRepository;
import ru.nsu.mordvinov.photobase.ui.MainLayout;
import ru.nsu.mordvinov.photobase.ui.crud.form.KioskForm;
import ru.nsu.mordvinov.photobase.ui.crud.grid.KioskGrid;
import ru.nsu.mordvinov.photobase.ui.crud.logic.KioskCrudLogic;
import ru.nsu.mordvinov.photobase.ui.crud.provider.KioskDataProvider;
import ru.nsu.mordvinov.photobase.ui.crud.view.base.BaseCrudView;

@Route(value = "Kiosk", layout = MainLayout.class)
@PageTitle("Kiosk")
public class KioskCrudView extends BaseCrudView<Kiosk> {

    public static final String VIEW_NAME = "Kiosk";


    private final KioskCrudLogic viewLogic;
    private final KioskRepository kioskRepository;
    private final BranchRepository branchRepository;

    @Autowired
    public KioskCrudView(KioskRepository kioskRepository,
                         BranchRepository branchRepository) {
        this.kioskRepository = kioskRepository;
        this.branchRepository = branchRepository;
        this.viewLogic = new KioskCrudLogic(this, kioskRepository, branchRepository);
        this.setRepository(kioskRepository);
        this.setViewLogic(viewLogic);
        this.setDataProvider(new KioskDataProvider(kioskRepository));
        this.setGrid(new KioskGrid());
        this.setForm(new KioskForm(viewLogic));
        this.setEntityName("kiosk");

        this.init();
        viewLogic.init();
    }


}
