package ru.nsu.mordvinov.photobase.backend.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nsu.mordvinov.photobase.TestHelper;
import ru.nsu.mordvinov.photobase.backend.model.Branch;
import ru.nsu.mordvinov.photobase.backend.model.Kiosk;
import ru.nsu.mordvinov.photobase.backend.model.Office;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RepositoryTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private KioskRepository kioskRepository;


    @Test
    @Transactional
    public void savingTest() {
        Office office = TestHelper.createOffice();
        office = officeRepository.save(office);
        assertNotNull(office.getId());

        Branch branch = TestHelper.createBranch();
        branch.setOffice(office);
        branch = branchRepository.save(branch);
        assertNotNull(branch.getId());
        assertEquals(branch.getOffice().getId(), office.getId());

        Kiosk kiosk = TestHelper.createKiosk();
        kiosk.setBranch(branch);
        kiosk = kioskRepository.save(kiosk);
        assertNotNull(kiosk.getId());
        assertEquals(kiosk.getBranch().getId(), branch.getId());
    }

}
